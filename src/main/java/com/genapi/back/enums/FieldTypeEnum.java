package com.genapi.back.enums;

public enum FieldTypeEnum {
    INTAGER,
    FLOAT,
    STRING,
    BOOLEAN,
    IMAGE;

    public static boolean contains(String test) {

        for (FieldTypeEnum c : FieldTypeEnum.values()) {
            if (c.name().equals(test)) {
                return true;
            }
        }

        return false;
    }
}
