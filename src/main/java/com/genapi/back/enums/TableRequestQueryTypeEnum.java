package com.genapi.back.enums;

public enum TableRequestQueryTypeEnum {

    CONTAINS,
    IN,
    EQUALS,
    BOOLEAN;

}
