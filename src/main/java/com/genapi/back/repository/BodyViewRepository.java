package com.genapi.back.repository;

import com.genapi.back.entity.BodyViewEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface BodyViewRepository extends JpaRepository<BodyViewEntity, Long> {
    BodyViewEntity getById(Long id);
    List<BodyViewEntity> findAllByProjectId(Long projectId);
    List<BodyViewEntity> findByModelId(Long modelId);
//    List<BodyViewEntity> getByArrayModelId(Long id);
}
