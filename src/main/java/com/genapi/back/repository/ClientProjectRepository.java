package com.genapi.back.repository;

import com.genapi.back.jooq.tables.pojos.ClientProject;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ClientProjectRepository extends JpaRepository<ClientProject, String> {
    ClientProject getAllByProjectId(Long prjectId);
    ClientProject findByClientId(String clientId);
    List<ClientProject> removeAllByProjectId(Long projectId);
}
