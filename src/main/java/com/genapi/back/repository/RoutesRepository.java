package com.genapi.back.repository;

import com.genapi.back.jooq.tables.pojos.Routes;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface RoutesRepository extends JpaRepository<Routes, Long> {
    List<Routes> findAllByProjectId(Long projectId);
    Routes findByNameAndProjectId(String name, Long projectId);
    Routes getById(Long id);
    Routes getByRouteAndProjectId(String route, Long projectId);
    void deleteAllByBodyId(Long bodyId);
    void deleteAllByListId(Long bodyId);
    List<Routes> removeAllByBodyId(Long bodyId);
    List<Routes> removeAllByListId(Long bodyId);
    Routes removeById(Long id);
    List<Routes> deleteAllByProjectId(Long projectId);

    @Modifying
    @Query(value = "DELETE FROM routes WHERE id IN (" +
            "SELECT r.id FROM routes AS r " +
            "LEFT JOIN body AS b ON b.id = r.body_id " +
            "LEFT JOIN lists AS l ON l.id = r.list_id " +
            "WHERE l.type LIKE CONCAT('MODEL(', ?1, ')%') OR b.model_id = ?1 " +
            ")", nativeQuery = true)
    void deleteAllByModelId(Long modelId);
}
