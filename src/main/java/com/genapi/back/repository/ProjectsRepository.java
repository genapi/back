package com.genapi.back.repository;

import com.genapi.back.jooq.tables.pojos.Projects;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProjectsRepository extends JpaRepository<Projects, Long> {
    boolean existsByNameAndUserId(String name, Long userId);
    boolean existsByIdAndUserId(Long id, Long userId);
    List<Projects> findAllByUserId(Long userId);
}
