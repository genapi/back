package com.genapi.back.annotations;

import com.genapi.back.enums.RolesEnum;
import com.genapi.back.utils.SecurityUtility;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import java.util.*;

@Aspect
@Component
public class HasRoleAspect {

    @Autowired
    SecurityUtility securityUtility;

    @Pointcut("@annotation(com.genapi.back.annotations.HasRole)")
    private void hasRoleAnnotation() {
    }

    @Around("@annotation(hr)")
    public Object doSomething(ProceedingJoinPoint pjp, HasRole hr) throws Throwable {

        RolesEnum[] roles = hr.value();
        if (roles == null || roles.length == 0) {
            return null;
        }

        boolean userHasRole = securityUtility.containsRole(roles);
        if (!userHasRole) {
            return ResponseEntity.status(HttpServletResponse.SC_FORBIDDEN).build();
        }

        return pjp.proceed();
    }

}
