package com.genapi.back.utils;

import com.genapi.back.dto.TableRequestDTO;
import com.genapi.back.dto.TableRequestQueryDTO;
import com.genapi.back.dto.TableResponseDTO;
import com.genapi.back.enums.TableRequestQueryTypeEnum;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class TableRequestBuilderUtility {

    public static TableResponseDTO buildRequest(List<String> columns, String table, TableRequestDTO request, JdbcTemplate jdbcTemplate) {
        String sql = "SELECT ";
        String countSql = "SELECT count(*) ";
        List<Object> queryObjects = new ArrayList<>();
        Long count = 0L;

        for (int i=0; i<columns.size(); i++) {
            sql += columns.get(i);
            if(i != columns.size() - 1) sql += ", ";
        }

        if(columns.size() == 0) sql += "*";

        sql += " FROM " + table;
        countSql += " FROM " + table;

        if(request.getQuery().size() != 0) {
            String where = " WHERE ";
            List<Object> whereObjects = new ArrayList<>();

            for (Map.Entry<String, TableRequestQueryDTO> whereQuery : request.getQuery().entrySet()) {

                if (whereQuery.getValue().getType() == TableRequestQueryTypeEnum.EQUALS) {
                    if (whereObjects.size() != 0) where += "AND ";
                    where += whereQuery.getKey() + " = ? ";
                    whereObjects.add(whereQuery.getValue().getQuery());
                }

                if (whereQuery.getValue().getType() == TableRequestQueryTypeEnum.CONTAINS) {
                    if (whereObjects.size() != 0) where += "AND ";
                    where += "strpos(" + whereQuery.getKey() + ", ?)>0 ";
                    whereObjects.add(whereQuery.getValue().getQuery());
                }

                if (whereQuery.getValue().getType() == TableRequestQueryTypeEnum.IN) {

                    List<Object> queryInList = (List<Object>) whereQuery.getValue().getQuery();

                    if (whereObjects.size() != 0) where += "AND ";
                    where += whereQuery.getKey() + " IN(";

                    for (Integer i = 0; i < queryInList.size(); i++) {
                        where += "?";
                        if (i != queryInList.size() - 1) where += ",";
                        whereObjects.add(queryInList.get(i));
                    }

                    where += ") ";
                }

            }

            sql += where;
            countSql += where;
            queryObjects.addAll(whereObjects);
        }

        if(request.getOrderBy() != null) {
            sql += " " + buildOrderBy(request.getOrderBy(), request.getAscending());
        }

        sql += " " + buildOffset(request.getPage(), request.getLimit());

        try {
            count = jdbcTemplate.queryForObject(countSql, queryObjects.toArray(), Long.class);
        } catch (DataAccessException e) {

        }

        return new TableResponseDTO(jdbcTemplate.queryForList(sql, queryObjects.toArray()), count);
    }

    public static TableResponseDTO buildRequest(String table, TableRequestDTO request, JdbcTemplate jdbcTemplate) {
        return buildRequest(new ArrayList<>(), table, request, jdbcTemplate);
    }

    public static String buildOffset(Long page, Long limit) {
        return "OFFSET " + (page-1) * limit + " LIMIT " + limit;
    }

    public static String buildOrderBy(String orderBy, Integer ascending) {
        return "ORDER BY " + orderBy + " " + buildAsc(ascending);
    }

    public static String buildAsc(Integer ascending) {
        return ascending.equals(1) ? "ASC" : "DESC";
    }

}
