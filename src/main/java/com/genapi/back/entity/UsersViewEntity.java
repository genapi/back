package com.genapi.back.entity;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Map;

@Data
@Entity
@Table(name = "users_view")
public class UsersViewEntity {

    @Id
    private Long id;
    private String login;
    private String password;
    private String email;
    private String name;
    private String role;
    private String clientId;
    private String additionalInformation;

}
