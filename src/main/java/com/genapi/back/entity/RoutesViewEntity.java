package com.genapi.back.entity;

import com.genapi.back.enums.ResponseTypeEnum;
import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "routes_view")
public class RoutesViewEntity {

    @Id
    private Long id;
    private String name;
    private String route;
    private Long projectId;
    private Long bodyId;
    private Long listId;
    private String responseType;
    private Long projectOwner;

    private boolean auth;
    private boolean active;

    public ResponseTypeEnum getResponseType() {
        return ResponseTypeEnum.valueOf(responseType);
    }

    public void setResponseType(ResponseTypeEnum responseType) {
        this.responseType = responseType.toString();
    }

}
