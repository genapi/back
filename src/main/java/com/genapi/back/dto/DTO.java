package com.genapi.back.dto;

import com.genapi.back.enums.ErrorsEnum;
import lombok.Data;
import org.springframework.beans.factory.annotation.Value;

@Data
public class DTO {

    @Value("#{new com.genapi.back.dto.ErrorBuilder()}")
    private ErrorBuilder errors;

    public void setErrors(ErrorBuilder errors) {
        this.errors = errors != null ? errors : new ErrorBuilder();
    }

    public boolean errorsIsExist() {
        return this.errors.getGeneral().size() != 0 || this.errors.getFields().size() != 0;
    }

    public void addError(ErrorsEnum error) {
        errors.add(error);
    }

    public void addError(String field, ErrorsEnum error) {
        errors.add(field, error);
    }
}
