package com.genapi.back.dto;

import com.genapi.back.enums.ErrorsEnum;
import lombok.Data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
public class ErrorBuilder {
    private List<ErrorsEnum> general = new ArrayList<>();
    private Map<String, List<ErrorsEnum>> fields = new HashMap<>();

    public void add(String field, ErrorsEnum error) {
        if(fields.get(field) == null) fields.put(field, new ArrayList<>());
        if(fields.get(field).contains(error)) return;
        fields.get(field).add(error);
    }

    public void add(ErrorsEnum error) {
        if(general.contains(error)) return;
        general.add(error);
    }
}
