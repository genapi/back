package com.genapi.back.dto;

import com.genapi.back.enums.ResponseTypeEnum;
import lombok.Data;

@Data
public class RouteDTO extends DTO {
    private Long id;
    private String name;
    private String route;
    private Long projectId;
    private Long bodyId;
    private Long listId;
    private ResponseTypeEnum responseType;
}
