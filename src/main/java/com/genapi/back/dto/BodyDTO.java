package com.genapi.back.dto;

import lombok.Data;
import org.bson.Document;

import java.util.List;


@Data
public class BodyDTO extends DTO {
    private Long   id;
    private String name;
    private Long   modelId;
    private Long   projectId;
    private Document value;
    private String modelName;
    private List<ModelFieldDTO> model;
    private String projectName;
    private Long projectOwner;
}
