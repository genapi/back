package com.genapi.back.dto;

import lombok.Data;

@Data
public class ProjectClientDTO extends DTO {

    private String clientId;
    private Long projectId;
    private String projectName;

}
