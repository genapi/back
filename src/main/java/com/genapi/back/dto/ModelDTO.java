package com.genapi.back.dto;

import lombok.Data;

import java.util.List;

@Data
public class ModelDTO extends DTO {
    private Long id;
    private String name;
    private Long projectId;
    private List<ModelFieldDTO> model;
    private Long bodyCount;
    private Long listCount;
}
