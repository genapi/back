package com.genapi.back.dto;

import com.genapi.back.enums.TableRequestQueryTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TableRequestQueryDTO {

    private TableRequestQueryTypeEnum type;
    private Object query;

}
