package com.genapi.back.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class TableResponseDTO {
    private List<Map<String, Object>> data;
    private Long count;
}
