package com.genapi.back.service;

import com.genapi.back.dto.*;
import com.genapi.back.dto.mapper.ModelsMapper;
import com.genapi.back.entity.ModelsViewEntity;
import com.genapi.back.enums.ErrorsEnum;
import com.genapi.back.enums.FieldTypeEnum;
import com.genapi.back.enums.TableRequestQueryTypeEnum;
import com.genapi.back.jooq.tables.pojos.Models;
import com.genapi.back.repository.ModelsRepository;
import com.genapi.back.repository.ModelsViewRepository;
import com.genapi.back.repository.ProjectsRepository;
import com.genapi.back.utils.TableRequestBuilderUtility;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static com.genapi.back.constants.RegexConstants.*;

@AllArgsConstructor
@Service
public class ModelsService {

    private final ModelsRepository modelsRepository;
    private final ModelsViewRepository modelsViewRepository;
    private final ProjectsRepository projectsRepository;
    private final ModelsMapper modelsMapper;
    private final JdbcTemplate jdbcTemplate;

    public ModelDTO create(ModelDTO modelDTO) throws IOException {

        if(modelDTO.getId() != null) {
            modelDTO.addError(ErrorsEnum.NOT_VALID_BODY);
            return modelDTO;
        }

        if(modelsRepository.existsByNameAndProjectId( modelDTO.getName(), modelDTO.getProjectId() )) {
            modelDTO.addError(ErrorsEnum.MATCH_NAME);
            return modelDTO;
        }

        validate(modelDTO);

        return save(modelDTO);
    }

    public ModelDTO update(ModelDTO modelDTO) throws IOException {

        if(modelDTO.getId() == null) {
            modelDTO.addError(ErrorsEnum.NOT_VALID_BODY);
            return modelDTO;
        }

        ModelsViewEntity model = modelsViewRepository.getByNameAndProjectId(modelDTO.getName(), modelDTO.getProjectId());

        if(model != null && !model.getId().equals(modelDTO.getId())) {
            modelDTO.addError(ErrorsEnum.MATCH_NAME);
            return modelDTO;
        }

        model = modelsViewRepository.getOne(modelDTO.getId());

        if((model.getBodyCount() != 0 || model.getListCount() != 0) && !model.getModel().replace(" ", "").equals(
                modelsMapper.dtoToEntity(modelDTO).getModel().replace(" ", "")
        )) {
            modelDTO.addError(ErrorsEnum.NOT_EDITABLE);
            return modelDTO;
        }

        validate(modelDTO);

        return save(modelDTO);

    }

    public TableResponseDTO getRows(TableRequestDTO request, Long projectOwner) {
        List<String> columns = new ArrayList<String>() {
            {
                add("id");
                add("name");
                add("project_id");
                add("model");
                add("body_count");
                add("list_count");
                add("project_name");
            }
        };
        request.getQuery().put("project_owner", new TableRequestQueryDTO(TableRequestQueryTypeEnum.EQUALS, projectOwner));
        return TableRequestBuilderUtility.buildRequest(columns, "models_view", request, jdbcTemplate);
    }

    public List<RowInfoDTO> getInfoRows(Long projectId) {
        return jdbcTemplate.query("SELECT id, name FROM models WHERE project_id = ?",
                new Object[]{projectId},
                new RowMapper<RowInfoDTO>() {
                    @Override
                    public RowInfoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new RowInfoDTO(rs.getLong(1), rs.getString(2));
                    }
                });
    }

    public List<RowInfoDTO> getAllInfoRows(Long projectOwner) {
        return jdbcTemplate.query("SELECT id, name FROM models_view WHERE project_owner = ?",
                new Object[]{projectOwner},
                new RowMapper<RowInfoDTO>() {
                    @Override
                    public RowInfoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                        return new RowInfoDTO(rs.getLong(1), rs.getString(2));
                    }
                });
    }

    public ModelDTO getById(Long id) throws IOException {
        ModelsViewEntity model = modelsViewRepository.getById(id);
        if(model == null) return null;
        return modelsMapper.viewToDto(model);
    }

    public void delete(Long id) {
        modelsRepository.deleteById(id);
    }

    public void deleteByProject(Long projectId) {
        modelsRepository.deleteAllByProjectId(projectId);
    }

    private ModelDTO validate(ModelDTO modelDTO) {

        String fieldType;
        Long fieldModelId = null;
        List<Long> modelIds = new ArrayList<>();
        List<String> names = new ArrayList<>();

        if(modelDTO.getName() == null || modelDTO.getName().length() == 0) {
            modelDTO.addError("name", ErrorsEnum.REQUIRED);
        }

        if(modelDTO.getProjectId() == null) {
            modelDTO.addError("projectId", ErrorsEnum.REQUIRED);
        } else if(!projectsRepository.existsById(modelDTO.getProjectId())) {
            modelDTO.addError("projectId", ErrorsEnum.NOT_MATCH);
        }

        if(modelDTO.getModel().size() == 0) {
            modelDTO.addError("model", ErrorsEnum.REQUIRED);
        }

        for (ModelFieldDTO field: modelDTO.getModel()) {
            fieldType = field.getType();

            if(!field.getName().matches(VALID_FIELD_NAME)) {
                modelDTO.addError("name", ErrorsEnum.REQUIRED);
            } else if(field.getName().equals("_id") || field.getName().equals("id")){
                modelDTO.addError("model", ErrorsEnum.NOT_VALID);
            } else if(names.contains(field.getName())){
                modelDTO.addError("model", ErrorsEnum.MATCH_FIELD_NAME);
            } else {
                names.add(field.getName());
            }

            if(!FieldTypeEnum.contains(fieldType) && fieldType.matches(VALID_TYPE_MODEL)) {
                fieldModelId = Long.valueOf(fieldType.replaceAll("MODEL|\\(|\\)", ""));
                if(!modelIds.contains(fieldModelId)) {
                    modelIds.add(
                            fieldModelId
                    );
                }
            } else if(!FieldTypeEnum.contains(fieldType) && fieldType.matches(VALID_TYPE_MODEL_LIST)) {
                fieldModelId = Long.valueOf(fieldType.replaceAll("MODEL|\\(|\\)\\[\\]", ""));
                if(!modelIds.contains(fieldModelId)) {
                    modelIds.add(
                            fieldModelId
                    );
                }
            } else if(
                    !fieldType.matches(VALID_TYPE_INTAGER_LIST) &&
                    !fieldType.matches(VALID_TYPE_FLOAT_LIST) &&
                    !fieldType.matches(VALID_TYPE_STRING_LIST) &&
                    !fieldType.matches(VALID_TYPE_BOOLEAN_LIST) &&
                    !fieldType.matches(VALID_TYPE_IMAGE_LIST) &&
                    !FieldTypeEnum.contains(fieldType)) {
                modelDTO.addError("model", ErrorsEnum.NOT_MATCH_MODEL);
            }
        }

        if(modelIds.contains(modelDTO.getId())) {
            modelDTO.addError("model", ErrorsEnum.RECURSIVE_PARAMETER);
        }

        if(modelIds.size() != 0) {
            List<Models> models = modelsRepository.findAllByIdInAndProjectId(modelIds, modelDTO.getProjectId());

            if(models.size() != modelIds.size()) {
                modelDTO.addError("model", ErrorsEnum.MODEL_BY_PROJECT);
            }
        }

        return modelDTO;
    }

    private ModelDTO save(ModelDTO modelDTO) throws IOException {
        if(modelDTO.errorsIsExist()) return modelDTO;

        Models model = modelsRepository.save(
                modelsMapper.dtoToEntity(modelDTO)
        );

        return modelsMapper.entityToDto(
                modelsRepository.save(model)
        );
    }

}
