package com.genapi.back.service;

import com.genapi.back.constants.RegexConstants;
import com.genapi.back.dto.*;
import com.genapi.back.dto.mapper.ProjectClientMapper;
import com.genapi.back.entity.ProjectClientsEntity;
import com.genapi.back.enums.ErrorsEnum;
import com.genapi.back.enums.TableRequestQueryTypeEnum;
import com.genapi.back.jooq.tables.pojos.ClientProject;
import com.genapi.back.jooq.tables.pojos.OauthClientDetails;
import com.genapi.back.repository.ClientProjectRepository;
import com.genapi.back.repository.OauthClientRepository;
import com.genapi.back.repository.ProjectClientsRepository;
import com.genapi.back.utils.ProjectClientIdUtility;
import com.genapi.back.utils.TableRequestBuilderUtility;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Service
public class ProjectClientsService {

    private final ClientProjectRepository clientProjectRepository;

    private final OauthClientRepository oauthClientRepository;

    private final ProjectClientsRepository projectClientsRepository;

    private final ProjectClientMapper projectClientMapper;

    private final UsersService usersService;

    private final BCryptPasswordEncoder passwordEncoder;

    private final JdbcTemplate jdbcTemplate;

    public List<RowInfoDTO> getInfoRows(Long userId) {
        return jdbcTemplate.query("SELECT client_id FROM project_clients WHERE project_owner = ?",
                new Object[]{userId},
                new RowMapper<RowInfoDTO>() {
                    @Override
                    public RowInfoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                        RowInfoDTO rowInfoDTO = new RowInfoDTO();
                        rowInfoDTO.setName(rs.getString(1));
                        return rowInfoDTO;
                    }
                }
        );
    }

    public List<RowInfoDTO> getInfoRowsByProject(Long projectId) {
        return jdbcTemplate.query("SELECT client_id FROM project_clients WHERE project_id = ?",
                new Object[]{projectId},
                new RowMapper<RowInfoDTO>() {
                    @Override
                    public RowInfoDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
                        RowInfoDTO rowInfoDTO = new RowInfoDTO();
                        rowInfoDTO.setName(rs.getString(1));
                        return rowInfoDTO;
                    }
                }
        );
    }

    public TableResponseDTO getRows(TableRequestDTO request, Long userId) {
        List<String> columns = new ArrayList<String>() {
            {
                add("client_id");
                add("project_name");
            }
        };
        request.getQuery().put("project_owner", new TableRequestQueryDTO(TableRequestQueryTypeEnum.EQUALS, userId));
        return TableRequestBuilderUtility.buildRequest(columns, "project_clients", request, jdbcTemplate);
    }

    public List<ProjectClientDTO> getClientsByProjectOwner(Long projectOwner) {
        return projectClientMapper.entityToDto(projectClientsRepository.getAllByProjectOwner(projectOwner));
    }

    public void delete(String clientId) {
        clientProjectRepository.deleteById(clientId);
        oauthClientRepository.deleteById(clientId);
    }

    public void deleteByProject(Long projectId) {
        List<String> ids = new ArrayList<>();

        clientProjectRepository.removeAllByProjectId(projectId).forEach(item -> {
            ids.add(item.getClientId());
        });

        if(ids.size() == 0) return;

        oauthClientRepository.deleteAllByClientIdIn(ids);
    }

    public ProjectClientDTO createClient(Long prjectId, String password) throws Exception {

        if(!password.matches(RegexConstants.VALID_PASSWORD)) {
            throw new Exception(ErrorsEnum.NO_VALID_PASSWORD.toString());
        }

        String clientId = ProjectClientIdUtility.generate(prjectId);

        OauthClientDetails oauthClientDetails = new OauthClientDetails();

        oauthClientDetails.setClientId(clientId);
        oauthClientDetails.setClientSecret(passwordEncoder.encode(password));
        oauthClientDetails.setScope("read");
        oauthClientDetails.setAuthorizedGrantTypes("password,refresh_token");
        oauthClientDetails.setAccessTokenValidity(3600);
        oauthClientDetails.setRefreshTokenValidity(2592000);
        oauthClientDetails.setAutoapprove("true");
        oauthClientDetails.setAdditionalInformation(String.format("{\"projectId\": %d}", prjectId));

        oauthClientRepository.save(oauthClientDetails);
        clientProjectRepository.save(new ClientProject(clientId, prjectId));

        return projectClientMapper.entityToDto(projectClientsRepository.getFirstByClientId(clientId));
    }

    public ClientProject getClientProject(String clientId) {
        return clientProjectRepository.findByClientId(clientId);
    }

}
