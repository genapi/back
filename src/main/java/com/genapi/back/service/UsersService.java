package com.genapi.back.service;

import com.genapi.back.constants.RegexConstants;
import com.genapi.back.dto.*;
import com.genapi.back.dto.mapper.UsersMapper;
import com.genapi.back.entity.ProjectUsersEntity;
import com.genapi.back.entity.UsersViewEntity;
import com.genapi.back.enums.ErrorsEnum;
import com.genapi.back.enums.RolesEnum;
import com.genapi.back.enums.TableRequestQueryTypeEnum;
import com.genapi.back.jooq.tables.pojos.Users;
import com.genapi.back.repository.*;
import com.genapi.back.utils.TableRequestBuilderUtility;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

import static com.genapi.back.constants.RegexConstants.VALID_EMAIL;
import static com.genapi.back.constants.RegexConstants.VALID_PASSWORD;

@AllArgsConstructor
@Service
public class UsersService {

    private UsersViewRepository usersViewRepository;
    private UsersRepository usersRepository;
    private RolesRepository rolesRepository;
    private ProjectUsersRepository projectUsersRepository;
    private ProjectClientsRepository projectClientsRepository;
    private BCryptPasswordEncoder passwordEncoder;
    private JdbcTemplate jdbcTemplate;

    private final UsersMapper usersMapper;

    public UserDTO getUserByLoginAndClientId(String login, String clientId) {
        return usersMapper.viewToDto(usersViewRepository.findFirstByLoginAndClientId(login, clientId));
    }

    public UserDTO getUserById(Long id) {
        return usersMapper.viewToDto(usersViewRepository.getOne(id));
    }

//    public UserDTO update(UserDTO userDTO) {
//        return save(userDTO);
//    }

    public UserDTO create(UserDetailsDTO userDetailsDTO) {

        if(userDetailsDTO.getId() != null) {
            userDetailsDTO.addError(ErrorsEnum.NOT_VALID_BODY);
            return usersMapper.detailsToDto(userDetailsDTO);
        }

        validate(userDetailsDTO);

        if(userDetailsDTO.errorsIsExist()) {
            return usersMapper.detailsToDto(userDetailsDTO);
        }

        Users user = usersMapper.detailsToEntity(userDetailsDTO);

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        user.setRoleId(rolesRepository.findByName(RolesEnum.USER.toString()).getId());

        user.setClientId("default");

        Long id = usersRepository.save(user).getId();

        return usersMapper.viewToDto(
                usersViewRepository.getOne(id)
        );
    }

    public ProjectClientUserDTO createForProjectClient(ProjectClientUserDetailsDTO projectClientUserDetailsDTO, Long projectOwner) {

        if(projectClientUserDetailsDTO.getClientId() == null || projectClientUserDetailsDTO.getClientId().equals("")) {
            projectClientUserDetailsDTO.addError("clientId", ErrorsEnum.REQUIRED);
        } else if(!projectClientsRepository.existsByClientIdAndProjectOwner(projectClientUserDetailsDTO.getClientId(), projectOwner)) {
            projectClientUserDetailsDTO.addError("clientId", ErrorsEnum.NOT_MATCH);
        }

        if(!projectClientUserDetailsDTO.errorsIsExist() && (projectClientUserDetailsDTO.getLogin() == null || projectClientUserDetailsDTO.getLogin().equals(""))) {
            projectClientUserDetailsDTO.addError("login", ErrorsEnum.REQUIRED);
        } else if(!projectClientUserDetailsDTO.getLogin().matches(RegexConstants.VALID_LOGIN)) {
            projectClientUserDetailsDTO.addError("login", ErrorsEnum.NOT_VALID);
        } else if(!projectClientUserDetailsDTO.errorsIsExist() && projectUsersRepository.existsByClientIdAndLogin(projectClientUserDetailsDTO.getClientId(), projectClientUserDetailsDTO.getLogin())) {
            projectClientUserDetailsDTO.addError(ErrorsEnum.MATCH_LOGIN);
        }

        if(!projectClientUserDetailsDTO.getPassword().matches(VALID_PASSWORD)) {
            projectClientUserDetailsDTO.addError("password", ErrorsEnum.NOT_VALID);
        }

        if(projectClientUserDetailsDTO.errorsIsExist()) {
            return usersMapper.projectClientUserDetailsToProjectClientUser(projectClientUserDetailsDTO);
        }

        Users user = usersMapper.projectClientUserDetailsToEntity(projectClientUserDetailsDTO);

        user.setRoleId(rolesRepository.findByName(RolesEnum.PROJECT_USER.toString()).getId());

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        usersRepository.save(user);

        return usersMapper.projectUsersEntityToProjectClientUser(
                projectUsersRepository.getOne( usersRepository.save(user).getId() )
        );

    }

    public List<ProjectClientUserDTO> getProjectClientUsersByClientId(String clientId, Long projectOwner) {
        return usersMapper.projectUsersEntityToProjectClientUser(projectUsersRepository.getAllByClientIdAndProjectOwner(clientId, projectOwner));
    }

    public TableResponseDTO getProjectClientUsersRows(TableRequestDTO request, Long projectOwner) {
        List<String> columns = new ArrayList<String>() {
            {
                add("id");
                add("client_id");
                add("login");
                add("project_name");
            }
        };
        request.getQuery().put("project_owner", new TableRequestQueryDTO(TableRequestQueryTypeEnum.EQUALS, projectOwner));
        return TableRequestBuilderUtility.buildRequest(columns, "project_users", request, jdbcTemplate);
    }

    public ProjectClientUserDTO getProjectClientUser(Long id, Long projectOwner) {
        return usersMapper.projectUsersEntityToProjectClientUser(projectUsersRepository.findByIdAndProjectOwner(id, projectOwner));
    }

    public UserDTO deleteUser(Long id) {
        UserDTO userDTO = usersMapper.viewToDto(usersViewRepository.getOne(id));
        usersRepository.deleteById(id);
        return userDTO;
    }

    public void deleteAllByClientId(String clientId) {
        usersRepository.deleteAllByClientId(clientId);
    }

    public void deleteProjectUserByProject(Long projectId) {
        List<Long> usersIds = new ArrayList<>();

        projectUsersRepository.getAllByProjectId(projectId).forEach(item -> {
            usersIds.add(item.getId());
        });

        if(usersIds.size() == 0) return;

        usersRepository.deleteAllByIdIn(usersIds);
    }

    public UserDetailsDTO validate(UserDetailsDTO userDetailsDTO) {

        if(userDetailsDTO.getName() == null || userDetailsDTO.getName().equals("")) {
            userDetailsDTO.addError("name", ErrorsEnum.REQUIRED);
        } else if(userDetailsDTO.getName().length() > 50) {
            userDetailsDTO.addError("name", ErrorsEnum.NOT_VALID);
        }

        if(userDetailsDTO.getLogin() == null || userDetailsDTO.getLogin().equals("")) {
            userDetailsDTO.addError("login", ErrorsEnum.REQUIRED);
        } else if(!userDetailsDTO.getLogin().matches(RegexConstants.VALID_LOGIN)) {
            userDetailsDTO.addError("login", ErrorsEnum.NOT_VALID);
        } else if(userDetailsDTO.getId() != null) {
            UsersViewEntity usersViewEntity = usersViewRepository.findFirstByLoginAndClientId(userDetailsDTO.getLogin(), "default");
            if(usersViewEntity != null && !usersViewEntity.getId().equals(userDetailsDTO.getId())) {
                userDetailsDTO.addError(ErrorsEnum.MATCH_LOGIN);
            }
        } else if(usersViewRepository.existsAllByLogin(userDetailsDTO.getLogin())) {
            userDetailsDTO.addError(ErrorsEnum.MATCH_LOGIN);
        }

        if(userDetailsDTO.getEmail() == null || userDetailsDTO.getEmail().equals("")) {
            userDetailsDTO.addError("email", ErrorsEnum.REQUIRED);
        } else if(!userDetailsDTO.getEmail().matches(VALID_EMAIL)) {
            userDetailsDTO.addError("email", ErrorsEnum.NOT_VALID);
        } else {
            Users user = usersRepository.getByEmail(userDetailsDTO.getEmail());
            if(user != null && userDetailsDTO.getId() != null && !userDetailsDTO.getId().equals(user.getId())) {
                userDetailsDTO.addError(ErrorsEnum.MATCH_EMAIL);
            } else if (userDetailsDTO.getId() == null && user != null) {
                userDetailsDTO.addError(ErrorsEnum.MATCH_EMAIL);
            }
        }

        if(userDetailsDTO.getYouAre() == null) {
            userDetailsDTO.addError("youAre", ErrorsEnum.REQUIRED);
        }

        if(userDetailsDTO.getCompanyName() == null) {
            userDetailsDTO.addError("companyName", ErrorsEnum.REQUIRED);
        }

        if(userDetailsDTO.getPassword() == null || userDetailsDTO.getPassword().equals("")) {
            userDetailsDTO.addError("password", ErrorsEnum.REQUIRED);
        } else if(!userDetailsDTO.getPassword().matches(VALID_PASSWORD)) {
            userDetailsDTO.addError("password", ErrorsEnum.NOT_VALID);
        }

        return userDetailsDTO;
    }

//    public UserDTO save(UserDetailsDTO userDetailsDTO) {
//        if(userDetailsDTO.errorsIsExist()) {
//            return usersMapper.detailsToDto(userDetailsDTO);
//        }
//
//        Users user = usersRepository.save(
//                usersMapper.detailsToEntity(userDetailsDTO)
//        );
//        return usersMapper.viewToDto(
//                usersViewRepository.findFirstByLogin(user.getLogin())
//        );
//    }

}
