package com.genapi.back.controller;

import com.genapi.back.annotations.HasRole;
import com.genapi.back.dto.BodyDTO;
import com.genapi.back.dto.RowInfoDTO;
import com.genapi.back.dto.TableRequestDTO;
import com.genapi.back.dto.TableResponseDTO;
import com.genapi.back.enums.RolesEnum;
import com.genapi.back.listeners.events.DeleteBodyEvent;
import com.genapi.back.service.BodyService;
import com.genapi.back.utils.SecurityUtility;
import lombok.AllArgsConstructor;
import org.bson.Document;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.List;

@AllArgsConstructor
@RequestMapping("/api/body")
@RestController
public class BodyController {

    private final BodyService bodyService;
    private final ProjectsController projectsController;
    private final ApplicationEventPublisher publisher;
    private final SecurityUtility securityUtility;

    @HasRole(RolesEnum.USER)
    @PostMapping("/create")
    public ResponseEntity<BodyDTO> create(@RequestBody BodyDTO bodyDTO) throws IOException {
        if(!projectsController.checkProject(bodyDTO.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(bodyService.create(bodyDTO));
    }

    @HasRole(RolesEnum.USER)
    @PutMapping("/update")
    public ResponseEntity<BodyDTO> update(@RequestBody BodyDTO bodyDTO) throws IOException {
        if(!projectsController.checkProject(bodyDTO.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(bodyService.update(bodyDTO));
    }

    @HasRole(RolesEnum.USER)
    @PostMapping("/get-rows")
    public ResponseEntity<TableResponseDTO> getRows(@RequestBody TableRequestDTO request) throws IOException {
        return ResponseEntity.ok(bodyService.getRows(request, securityUtility.getUserId()));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/get-info-rows")
    public ResponseEntity<List<RowInfoDTO>> getInfoRows(@RequestParam Long projectId) {
        if(!projectsController.checkProject(projectId)) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(bodyService.getInfoRows(projectId));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/get-by-model")
    public ResponseEntity<List<BodyDTO>> getByModel(@RequestParam Long projectId, @RequestParam Long modelId) throws IOException {
        if(!projectsController.checkProject(projectId)) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(bodyService.getByModel(modelId));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/{id}")
    public ResponseEntity<BodyDTO> getById(@PathVariable Long id) throws IOException {
        BodyDTO bodyDTO = bodyService.getById(id);
        if(bodyDTO == null || !projectsController.checkProject(bodyDTO.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(bodyDTO);
    }

    /*@PreAuthorize("hasAuthority('USER')")
    @GetMapping("/by-array-model/{id}")
    public ResponseEntity<List<BodyDTO>> getByArrayModelId(@PathVariable Long id) throws IOException {
        List<BodyDTO> bodyDTOS = bodyService.getByArrayModelId(id);
        if(bodyDTOS == null || !projectsController.checkProject(bodyDTOS.get(0).getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(bodyDTOS);
    }*/

    @HasRole(RolesEnum.USER)
    @GetMapping("/preview/{id}")
    public ResponseEntity<Document> getPreviewById(@PathVariable Long id) throws IOException {
        BodyDTO bodyDTO = bodyService.getById(id);
        if(bodyDTO == null || !projectsController.checkProject(bodyDTO.getProjectId())) return ResponseEntity.badRequest().build();
        return ResponseEntity.ok(bodyService.getPreviewById(id));
    }

    @HasRole(RolesEnum.USER)
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<BodyDTO> delete(@PathVariable Long id) throws IOException {
        BodyDTO bodyDTO = bodyService.getById(id);
        if(bodyDTO == null || !projectsController.checkProject(bodyDTO.getProjectId())) return ResponseEntity.badRequest().build();
        publisher.publishEvent(new DeleteBodyEvent(id));
        return ResponseEntity.ok(bodyDTO);
    }

}
