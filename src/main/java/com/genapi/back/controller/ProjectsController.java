package com.genapi.back.controller;

import com.genapi.back.annotations.HasRole;
import com.genapi.back.dto.*;
import com.genapi.back.enums.RolesEnum;
import com.genapi.back.listeners.events.DeleteProjectEvent;
import com.genapi.back.service.ProjectsService;
import com.genapi.back.service.UsersService;
import com.genapi.back.utils.SecurityUtility;
import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RequestMapping("/api/projects")
@RestController
public class ProjectsController {

    private final ProjectsService projectsService;
    private final UsersService usersService;
    private final SecurityUtility securityUtility;
    private final ApplicationEventPublisher publisher;

    @HasRole(RolesEnum.USER)
    @PostMapping("/create")
    public ResponseEntity<ProjectDTO> create(@RequestBody ProjectDTO projectDTO) {
        return ResponseEntity.ok(projectsService.create(projectDTO));
    }

    @HasRole(RolesEnum.USER)
    @PutMapping("/update")
    public ResponseEntity<ProjectDTO> update(@RequestBody ProjectDTO projectDTO) {
        if(!checkProject(projectDTO.getId())) ResponseEntity.badRequest().build();
        return ResponseEntity.ok(projectsService.update(projectDTO));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/get-info-rows")
    public ResponseEntity<List<RowInfoDTO>> getInfoRows() {
        return ResponseEntity.ok(projectsService.getInfoRows(securityUtility.getUserId()));
    }
    @HasRole(RolesEnum.USER)
    @GetMapping("/get-info-rows-by-model")
    public ResponseEntity<List<RowInfoDTO>> getInfoRowsByModel(@RequestParam Long modelId) {
        return ResponseEntity.ok(projectsService.getInfoRowsByModel(modelId, securityUtility.getUserId()));
    }

    @HasRole(RolesEnum.USER)
    @PostMapping("/get-rows")
    public ResponseEntity<TableResponseDTO> getRows(@RequestBody TableRequestDTO request) {
        return ResponseEntity.ok(projectsService.getRows(request, securityUtility.getUserId()));
    }

    @HasRole(RolesEnum.USER)
    @GetMapping("/{id}")
    public ResponseEntity<ProjectDTO> getById(@PathVariable Long id) {
        if(!checkProject(id)) ResponseEntity.badRequest().build();
        return ResponseEntity.ok(projectsService.getById(id));
    }

    @HasRole(RolesEnum.USER)
    @DeleteMapping("/delete/{id}")
    public ResponseEntity<ProjectDTO> delete(@PathVariable Long id) {
        if(!checkProject(id)) ResponseEntity.badRequest().build();
        ProjectDTO projectDTO = projectsService.getById(id);
        publisher.publishEvent(new DeleteProjectEvent(id));
        return ResponseEntity.ok(projectDTO);
    }

    public Boolean checkProject(Long projectId) {
        ProjectDTO projectDTO = projectsService.getById(projectId);
        if(projectDTO == null) return false;
        if(!projectDTO.getUserId().equals(securityUtility.getUserId())) return false;
        return true;
    }

}
