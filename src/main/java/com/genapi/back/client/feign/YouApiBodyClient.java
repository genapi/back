package com.genapi.back.client.feign;

import org.bson.Document;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@FeignClient(name = "you-api")
@RequestMapping("/you-api/internal/body")
public interface YouApiBodyClient {

    @GetMapping("/get-for-body/{id}")
    Document getByIdForBody(@PathVariable Long id);

    @GetMapping("/get-preview/{id}")
    Document getPreviewById(@PathVariable Long id);

    @PostMapping("/save")
    Document save(@RequestParam Long id, @RequestParam String value, @RequestParam String model);

    @GetMapping("/get-for-list/{id}")
    List<Object> getByIdForList(@PathVariable Long id);

    @GetMapping("/get-preview-list/{id}")
    List<Object> getPreviewListById(@PathVariable Long id);

    @PostMapping("/save-list")
    List<Object> saveList(@RequestParam Long id, @RequestParam String list, @RequestParam String type);

    @DeleteMapping("/delete-body/{id}")
    boolean delete(@PathVariable Long id);

    @DeleteMapping("/delete-body")
    boolean delete(@RequestParam List<Long> ids);

    @DeleteMapping("/delete-list/{id}")
    boolean deleteList(@PathVariable Long id);

    @DeleteMapping("/delete-list")
    boolean deleteList(@RequestParam List<Long> ids);

}
