package com.genapi.back.filters;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class FeignFilter implements Filter {

    private static final String AUTHORIZATION_HEADER = "Server-Token";

    private static final String BEARER_TOKEN_TYPE = "Bearer";

    @Value("${spring.security.server-key}")
    private String secret;

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest request = (HttpServletRequest)servletRequest;
        HttpServletResponse response = (HttpServletResponse)servletResponse;

        if(checkUrl(request.getRequestURI())) {

            String header = request.getHeader(AUTHORIZATION_HEADER);

            if(header == null) {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return;
            }

            if(!checkAuthHeader(header)) {
                response.setStatus(HttpServletResponse.SC_FORBIDDEN);
                return;
            }

        }

        filterChain.doFilter(request, response);
    }

    private Boolean checkUrl(String url) {
        return url.matches("^/internal/\\S+$");
    }

    private Boolean checkAuthHeader(String header) {
        String token = header.replace(" ", "").replace(BEARER_TOKEN_TYPE, "");
        return passwordEncoder.matches(secret, token);
    }

    public void init(FilterConfig filterConfig) {}

    public void destroy() {}

}
