CREATE TABLE projects
(
  id              BIGSERIAL,
  name            VARCHAR(50) NOT NULL,
  user_login      VARCHAR(50),
  auth            BOOLEAN DEFAULT TRUE,
  active          BOOLEAN DEFAULT TRUE,

  PRIMARY KEY (id),
  UNIQUE (name, user_login),
  FOREIGN KEY (user_login) REFERENCES users (login)
)