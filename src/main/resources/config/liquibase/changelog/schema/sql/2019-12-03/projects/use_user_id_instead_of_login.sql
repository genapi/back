ALTER TABLE projects DROP CONSTRAINT IF EXISTS projects_name_user_login_key;
-- ALTER TABLE projects DROP COLUMN user_login;
ALTER TABLE projects ADD COLUMN user_id BIGINT;

UPDATE projects SET user_id = 1;

ALTER TABLE projects ALTER COLUMN user_id SET NOT NULL;
ALTER TABLE projects ADD CONSTRAINT projects_name_user_id_key UNIQUE (name, user_id);
