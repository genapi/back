CREATE TABLE client_project (
  client_id     VARCHAR(256) NOT NULL PRIMARY KEY,
  project_id    BIGINT NOT NULL,

  FOREIGN KEY (client_id) REFERENCES oauth_client_details (client_id),
  FOREIGN KEY (project_id) REFERENCES projects (id)
)