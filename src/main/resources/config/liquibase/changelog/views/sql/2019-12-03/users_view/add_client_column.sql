DROP VIEW IF EXISTS users_view;
CREATE VIEW users_view AS (SELECT u.*,
                                    c.additional_information,
                                    r.name as role
                              FROM users u
                              LEFT JOIN roles r ON r.id = u.role_id
                              LEFT JOIN oauth_client_details c ON c.client_id = u.client_id
                              GROUP BY u.id, u.login, u.password, r.name, c.additional_information
                              ORDER BY u.id
);
