CREATE VIEW users_view AS (SELECT u.id,
                                    u.login,
                                    u.password,
                                    u.email,
                                    u.name,
                                    r.name as role
                              FROM users u
                              LEFT JOIN roles r ON r.id = u.role_id
                              GROUP BY u.id, u.login, u.password, r.name
                              ORDER BY u.id
);
