DROP VIEW IF EXISTS models_view;
CREATE VIEW models_view AS (SELECT m.*,
                                   count(b.id) as body_count,
                                   count(l.id) as list_count,
                                   p.user_id AS project_owner,
                                   p.name AS project_name
                            FROM models m
                                   LEFT JOIN body b ON b.model_id = m.id
                                   LEFT JOIN lists l ON l.type LIKE CONCAT('MODEL(', m.id, ')%')
                                   LEFT JOIN projects p ON p.id = m.project_id
                            GROUP BY m.id, p.user_id, p.name
                            ORDER BY m.id)