DROP VIEW IF EXISTS models_view;
CREATE VIEW models_view AS (SELECT m.*,
       (SELECT count(b.id) FROM body b WHERE b.model_id = m.id) as body_count,
       (SELECT count(l.id) FROM lists l WHERE l.type LIKE CONCAT('MODEL(', m.id, ')%')) as list_count,
       p.user_id AS project_owner,
       p.name AS project_name
FROM models m
       LEFT JOIN projects p ON p.id = m.project_id
GROUP BY m.id, p.user_id, p.name
ORDER BY m.id)